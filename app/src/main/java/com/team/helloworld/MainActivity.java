package com.team.helloworld;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    int count;
    String check;
    Bundle bundle;
    DatabaseReference databaseReference;
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser!=null) {
            flag = true;

            check = Boolean.toString(flag);
            count = 0;
            bundle = new Bundle();
            bundle.putString("CHECK", check);

            databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getUid());
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    String editName = user.getName();
                    String editEmail = user.geteMail();

                    bundle.putString("NAME", editName);
                    bundle.putString("EMAIL", editEmail);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.menu);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_holder, new HomeFragment()).commit();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {
                        case R.id.nav_home :
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.nav_trivia :
                            selectedFragment = new TriviaFragment();
                            break;
                        case R.id.nav_projects :
                            selectedFragment = new ProjectsFragment();
                            break;
                        case R.id.nav_settings :
                            selectedFragment = new SettingsFragment();
                            selectedFragment.setArguments(bundle);
                            break;
                    }

                    count = 0;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_holder, selectedFragment).commit();
                    return true;
                }
            };

    @Override
    protected void onResume() {
        count = 0;
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if(count==1){
            finishAffinity();
        } else {
            Toast.makeText(MainActivity.this,"Press BACK again to exit",Toast.LENGTH_SHORT).show();
            count++;
        }
    }
}
