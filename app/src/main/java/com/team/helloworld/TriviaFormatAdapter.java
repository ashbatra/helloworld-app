package com.team.helloworld;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class TriviaFormatAdapter extends ArrayAdapter<Question> {

    private Activity context;
    private List<Question> questions;

    public TriviaFormatAdapter(Activity context, List<Question> questions) {
        super(context, R.layout.list_item_layout, questions);

        this.context = context;
        this.questions = questions;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();

        View listViewItem = layoutInflater.inflate(R.layout.list_item_layout, null, true);

        TextView imageView = listViewItem.findViewById(R.id.number);
        TextView txt = listViewItem.findViewById(R.id.small_title);
        Question question = questions.get(position);

        int number = position + 1;
        String no = Integer.toString(number);
        imageView.setText(no);
        txt.setText(question.getSmallTitle());

        return listViewItem;
    }
}
