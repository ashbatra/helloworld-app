package com.team.helloworld;

public class Link {
    String type;
    String link;

    public Link() {

    }

    public Link(String type, String link) {
        this.type = type;
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public String getLink() {
        return link;
    }
}
