package com.team.helloworld;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TriviaFragment extends Fragment {

    Button SignUp, previousTrivia;
    RelativeLayout trivia, trivia_logout, triviacard;
    FloatingActionButton leaderboard, addTrivia;
    ProgressBar progressBar;
    LinearLayout no_trivia;
    TextView start_trivia, declare_winner, title, noofques, maxTime, active_till, Level;
    List<Question> questionList;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trivia, container, false);

        trivia = view.findViewById(R.id.trivia);
        trivia_logout = view.findViewById(R.id.logout_trivia);
        triviacard = view.findViewById(R.id.trivia_card);
        no_trivia = view.findViewById(R.id.no_trivia);
        leaderboard = view.findViewById(R.id.leaderboard);
        title = view.findViewById(R.id.title);
        noofques = view.findViewById(R.id.no_of_ques);
        maxTime = view.findViewById(R.id.max_time);
        active_till = view.findViewById(R.id.active_till);
        Level= view.findViewById(R.id.level);
        declare_winner = view.findViewById(R.id.declare_winner);
        addTrivia = view.findViewById(R.id.create_trivia);
        start_trivia = view.findViewById(R.id.start_trivia);
        previousTrivia = view.findViewById(R.id.prev_trivia);
        progressBar = view.findViewById(R.id.progressBar_trivia);

        previousTrivia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // redirect to an activity containing trivias
                Intent intent = new Intent(getContext(), TriviaListActivity.class);
                intent.putExtra("CALLER", "Previous Trivia");
                startActivity(intent);

            }
        });

        leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // redirect to an activity containing trivias
                Intent intent = new Intent(getContext(), TriviaListActivity.class);
                intent.putExtra("CALLER", "LeaderBoard");
                startActivity(intent);
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        if(firebaseUser == null) {
            progressBar.setVisibility(View.GONE);
            trivia_logout.setVisibility(View.VISIBLE);
        }
        else {

            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseAuth.getUid());

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

                @SuppressLint("RestrictedApi")
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    User user = dataSnapshot.getValue(User.class);
                    String isCore = user.getIsTriviaCore();
                    final UserQuizData quizData = user.getQuizData();

                    if (isCore.equals("true")) {
                        start_trivia.setVisibility(View.GONE);

                        databaseReference = firebaseDatabase.getReference().child("Trivia");

                            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    TriviaFormat triviaFormat = dataSnapshot.getValue(TriviaFormat.class);
                                    if(triviaFormat==null) {
                                        progressBar.setVisibility(View.GONE);
                                        triviacard.setVisibility(View.GONE);
                                        trivia.setVisibility(View.VISIBLE);
                                        no_trivia.setVisibility(View.VISIBLE);
                                        addTrivia.setVisibility(View.VISIBLE);

                                        addTrivia.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                startActivity(new Intent(getContext(), AddTriviaActivity.class));
                                            }
                                        });


                                    } else {

                                        Date expDate = triviaFormat.getExpDate();
                                        Date activeTill = triviaFormat.getActiveTill();
                                        active_till.setText("*  Trivia active till : " + DateFormat.getInstance().format(activeTill));
                                        title.setText(triviaFormat.getTitle());
                                        noofques.setText("*  No. of Questions : " + triviaFormat.getNoOfQues());
                                        maxTime.setText("*  Time allotted : " + triviaFormat.getTime() + " mins");
                                        Level.setText("*  Level : " + triviaFormat.getLevel());


                                        Calendar calendar = Calendar.getInstance();
                                        Date date = calendar.getTime();
                                        if (date.after(expDate) || date.equals(expDate)) {
                                            shiftTrivia(triviaFormat);
                                            triviacard.setVisibility(View.GONE);
                                            no_trivia.setVisibility(View.VISIBLE);
                                            progressBar.setVisibility(View.GONE);
                                            trivia.setVisibility(View.VISIBLE);
                                            addTrivia.setVisibility(View.VISIBLE);

                                            addTrivia.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    startActivity(new Intent(getContext(), AddTriviaActivity.class));
                                                }
                                            });

                                        } else {
                                            triviacard.setVisibility(View.VISIBLE);
                                            no_trivia.setVisibility(View.GONE);
                                            progressBar.setVisibility(View.GONE);
                                            trivia.setVisibility(View.VISIBLE);
                                            addTrivia.setVisibility(View.VISIBLE);

                                            addTrivia.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Toast toast= Toast.makeText(getContext(), "You can't add a trivia when there's one trivia live", Toast.LENGTH_SHORT);
                                                    toast.setGravity(Gravity.BOTTOM, 0, 300);
                                                    toast.show();
                                                }
                                            });

                                            if (date.after(activeTill) || date.equals(activeTill)) {
                                                declare_winner.setVisibility(View.VISIBLE);
                                                declare_winner.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Toast.makeText(getContext(), "Opening list...", Toast.LENGTH_SHORT).show();
                                                        startActivity(new Intent(getContext(), UserQuizDataListActivity.class));
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                    } else if (isCore.equals("false")) {
                        databaseReference = firebaseDatabase.getReference().child("Trivia");

                        databaseReference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                final TriviaFormat triviaFormat = dataSnapshot.getValue(TriviaFormat.class);
                                if(triviaFormat == null) {
                                    triviacard.setVisibility(View.GONE);
                                    trivia.setVisibility(View.VISIBLE);
                                    no_trivia.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    leaderboard.setVisibility(View.VISIBLE);
                                }
                                else {

                                    Date expDate = triviaFormat.getExpDate();
                                    Date activeTill = triviaFormat.getActiveTill();
                                    active_till.setText("*  Trivia active till : " + DateFormat.getInstance().format(activeTill));
                                    title.setText(triviaFormat.getTitle());
                                    noofques.setText("*  No. of Questions : " + triviaFormat.getNoOfQues());
                                    maxTime.setText("*  Time allotted : " + triviaFormat.getTime() + " mins");
                                    Level.setText("*  Level : " + triviaFormat.getLevel());

                                    Calendar calendar = Calendar.getInstance();
                                    Date date = calendar.getTime();

                                    if (date.after(expDate) || date.equals(expDate)) {
                                        shiftTrivia(triviaFormat);
                                        triviacard.setVisibility(View.GONE);
                                        trivia.setVisibility(View.VISIBLE);
                                        no_trivia.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.GONE);
                                        leaderboard.setVisibility(View.VISIBLE);

                                    } else {
                                        triviacard.setVisibility(View.VISIBLE);
                                        no_trivia.setVisibility(View.GONE);
                                        trivia.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.GONE);
                                        leaderboard.setVisibility(View.VISIBLE);

                                        if (quizData != null) {
                                            start_trivia.setVisibility(View.GONE);

                                        } else {

                                            if (date.after(activeTill) || date.equals(activeTill)) {
                                                start_trivia.setVisibility(View.GONE);
                                            }

                                            else {
                                                start_trivia.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        questionList = triviaFormat.getQuestions();
                                                        // open triviaActivity and in intentExtra, pass forPlaying purpose
                                                        Intent intent = new Intent(getContext(), TriviaActivity.class);
                                                        intent.putExtra("USE_OF_ACTIVITY", "Play");
                                                        startActivity(intent);
                                                        getActivity().finishAffinity();
                                                    }
                                                });
                                            }
                                        }

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

        SignUp = view.findViewById(R.id.btn1);
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                TabbedDialog dialogFragment = new TabbedDialog();
                dialogFragment.show(ft,"dialog");

            }
        });

        return view;
    }

    private void shiftTrivia(TriviaFormat triviaFormat) {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Previous Trivia");
        databaseReference.child(triviaFormat.getTitle()).setValue(triviaFormat);
        databaseReference = firebaseDatabase.getReference().child("Trivia");
        databaseReference.removeValue();

        databaseReference = firebaseDatabase.getReference().child("Users");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    String user = userSnapshot.getKey();

                    databaseReference = firebaseDatabase.getReference().child("Users").child(user).child("quizData");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            UserQuizData userQuizData = dataSnapshot.getValue(UserQuizData.class);
                            if(userQuizData == null) {
                                return;
                            }
                            else {
                                databaseReference.removeValue();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
