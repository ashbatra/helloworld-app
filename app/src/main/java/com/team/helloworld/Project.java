package com.team.helloworld;

import java.util.List;

public class Project {
    private String title;
    private String description;
    private String field;
    private String mentor;
    private List<Link> links;

    public Project() {

    }

    public Project(String title, String description, String field, String mentor, List<Link> links) {
        this.title = title;
        this.description = description;
        this.field = field;
        this.mentor = mentor;
        this.links = links;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getField() {
        return field;
    }

    public String getMentor() {
        return mentor;
    }

    public List<Link> getLinks() {
        return links;
    }
}

