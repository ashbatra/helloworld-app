package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TriviaListActivity extends AppCompatActivity {
    ProgressBar progressBar;
    RelativeLayout noTrivia;
    ListView listView;
    TextView heading;
    ImageView imageView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    List<TriviaFormat> triviaList;
    String intentExtra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trivia_list);

        getSupportActionBar().hide();

        intentExtra = getIntent().getStringExtra("CALLER");

        triviaList = new ArrayList<>();
        noTrivia = findViewById(R.id.no_recent_trivia);
        progressBar = findViewById(R.id.progressBar_trivia_list);
        listView = findViewById(R.id.trivia_list);
        imageView = findViewById(R.id.back_btn);
        heading = findViewById(R.id.heading_trivia_list);

        heading.setText(intentExtra);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Previous Trivia");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                triviaList.clear();
                for(DataSnapshot triviaSnapshot : dataSnapshot.getChildren()) {
                    TriviaFormat triviaFormat = triviaSnapshot.getValue(TriviaFormat.class);

                        triviaList.add(triviaFormat);
                        listView.setVisibility(View.VISIBLE);
                }

                TriviaFormat triviaFormat1 = dataSnapshot.getValue(TriviaFormat.class);
                if(triviaFormat1 == null) {
                    noTrivia.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                TriviaAdapter triviaAdapter = new TriviaAdapter(TriviaListActivity.this, triviaList);
                listView.setAdapter(triviaAdapter);

                if(intentExtra.equals("Previous Trivia")) {
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(TriviaListActivity.this, TriviaActivity.class);
                            intent.putExtra("USE_OF_ACTIVITY", "View");
                            intent.putExtra("TRIVIA_NAME", triviaList.get(position).getTitle());
                            startActivity(intent);
                        }
                    });
                }

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(TriviaListActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
