package com.team.helloworld;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuizData {
    static Date startedAt;
    static Date finishedAt;
    static List<Boolean> correctOrNOt;

    public static Date getStartedAt() {
        return startedAt;
    }

    public static void setStartedAt(Date startedAt) {
        QuizData.startedAt = startedAt;
    }

    public static Date getFinishedAt() {
        return finishedAt;
    }

    public static void setFinishedAt(Date finishedAt) {
        QuizData.finishedAt = finishedAt;
    }

    public static List<Boolean> getCorrectOrNOt() {
        return correctOrNOt;
    }

    public static void setCorrectOrNOt(int number) {
        correctOrNOt = new ArrayList<>(number);

        for(int i=0;i<number;i++) {
            correctOrNOt.add(false);
        }
    }
}
