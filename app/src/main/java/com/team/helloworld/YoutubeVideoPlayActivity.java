package com.team.helloworld;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class YoutubeVideoPlayActivity extends YouTubeBaseActivity {

    YouTubePlayerView youTubePlayerView;
    YouTubePlayer.OnInitializedListener onInitializedListener;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_video_play);

        final String url = getIntent().getStringExtra("VIDEO_URL");

        youTubePlayerView = findViewById(R.id.youtube_Player);

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(getId(url));
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        youTubePlayerView.initialize(YouTubeConfiguration.getApiKey(), onInitializedListener);
    }

    private String getId(String url) {
        char[] URL = url.toCharArray();
        char[] ID = new char[11];

        for (int i=0;i<ID.length;i++) {
            ID[i] = URL[i+17];
        }

        String id = new String(ID);
        id = id.trim();
        return id;
    }
}
