package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProjectListActivity extends AppCompatActivity {
    TextView actionBar, noProject;
    ImageView back;
    ProgressBar progressBar;
    ListView listView;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    List<Project> projectsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);

        getSupportActionBar().hide();
        projectsList = new ArrayList<>();
        actionBar = findViewById(R.id.actionBar_title);
        noProject =  findViewById(R.id.no_projects);
        back = findViewById(R.id.back_btn_project_list);
        progressBar = findViewById(R.id.progressBar_projects_list);
        listView = findViewById(R.id.projects_list);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String number = getIntent().getStringExtra("Card Number");
        String key = "";

        if(number.equals("0")){
            actionBar.setText("C/C++ Projects");
            key = "C and C++";
        } else if(number.equals("1")) {
            actionBar.setText("Graphics Projects");
            key = "Graphics";
        } else if(number.equals("2")) {
            actionBar.setText("Ethical Hacking Projects");
            key = "Ethical Hacking";
        } else if(number.equals("3")) {
            actionBar.setText("Python Projects");
            key = "Python";
        } else if(number.equals("4")) {
            actionBar.setText("ML Projects");
            key = "Machine Learning";
        } else if(number.equals("5")) {
            actionBar.setText("Data Science Projects");
            key = "Data Science";
        } else if(number.equals("6")) {
            actionBar.setText("Android (Java) Projects");
            key = "Android (Java)";
        } else if(number.equals("7")) {
            actionBar.setText("Android (Flutter) Projects");
            key = "Android (Flutter)";
        } else if(number.equals("8")) {
            actionBar.setText("WebD Projects");
            key = "WebD";
        }

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Projects").child(key);

        final String finalKey = key;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Project project = dataSnapshot.getValue(Project.class);
                if(project == null) {
                    noProject.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                projectsList.clear();
                for(DataSnapshot projectSnapshot : dataSnapshot.getChildren()) {
                    Project project1 = projectSnapshot.getValue(Project.class);

                    projectsList.add(project1);
                    listView.setVisibility(View.VISIBLE);
                }

                ProjectAdapter projectAdapter = new ProjectAdapter(ProjectListActivity.this, projectsList);
                listView.setAdapter(projectAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String title = projectsList.get(position).getTitle();
                        Intent intent =  new Intent(ProjectListActivity.this, ProjectActivity.class);
                        intent.putExtra("FIELD", finalKey);
                        intent.putExtra("PROJECT_TITLE", title);
                        startActivity(intent);
                    }
                });

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProjectListActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
