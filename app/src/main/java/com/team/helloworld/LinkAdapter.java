package com.team.helloworld;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class LinkAdapter extends ArrayAdapter<Link> {

    private Activity context;
    private List<Link> links;

    public LinkAdapter(Activity context, List<Link> links) {
        super(context, R.layout.list_item_layout, links);

        this.context = context;
        this.links = links;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();

        View listViewItem = layoutInflater.inflate(R.layout.list_item_layout_2, null, true);

        TextView imageView = listViewItem.findViewById(R.id.number);
        TextView txt = listViewItem.findViewById(R.id.small_title);
        String link = links.get(position).getLink();

        int number = position + 1;
        String no = Integer.toString(number);
        imageView.setText(no);
        txt.setText(link);

        return listViewItem;
    }
}
