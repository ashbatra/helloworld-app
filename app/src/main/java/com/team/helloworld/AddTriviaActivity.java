package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddTriviaActivity extends AppCompatActivity {

    EditText editTitle, editNo, editTime;
    EditText quesTitle, quesQuestion, quesImgLink, quesHint, quesAnswer, quesApproach;
    Spinner level;
    ListView listView;
    Button addQuestion, addTrivia, add, cancel;
    ImageView back;
    List<Question> list;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trivia_);
        getSupportActionBar().hide();

        editTitle = findViewById(R.id.iquiz_title);
        editNo = findViewById(R.id.inoofquestions);
        editTime = findViewById(R.id.imaxtime);
        level = findViewById(R.id.ilevel);
        back = findViewById(R.id.back_pp);
        list = new ArrayList<>();
        builder = new AlertDialog.Builder(this);
        addQuestion = findViewById(R.id.add_question);
        listView = findViewById(R.id.question_list);
        addTrivia = findViewById(R.id.add_trivia_btn);
        final TriviaFormatAdapter questionAdapter = new TriviaFormatAdapter(AddTriviaActivity.this, list);
        listView.setAdapter(questionAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setMessage("All the details of trivia saved will be lost. You will have to enter the details again.")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Are you sure you want to exit?");
                alert.show();

            }
        });

        addQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog_add = new Dialog(AddTriviaActivity.this);
                dialog_add.setContentView(getLayoutInflater().inflate(R.layout.question_dialog, null));
                dialog_add.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                quesTitle = dialog_add.findViewById(R.id.i_question_title);
                quesQuestion = dialog_add.findViewById(R.id.i_question);
                quesImgLink = dialog_add.findViewById(R.id.i_image_link);
                quesHint = dialog_add.findViewById(R.id.i_hint);
                quesAnswer = dialog_add.findViewById(R.id.i_answer);
                quesApproach = dialog_add.findViewById(R.id.i_approach);

                add= dialog_add.findViewById(R.id.add_btn);
                cancel = dialog_add.findViewById(R.id.cancel_btn);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_add.dismiss();
                    }
                });

                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String str1 = quesTitle.getText().toString().trim();
                        String str2 = quesQuestion.getText().toString().trim();
                        String str3 = quesImgLink.getText().toString().trim();
                        String str4 = quesHint.getText().toString().trim();
                        String str5 = quesAnswer.getText().toString().trim();
                        String str6 = quesApproach.getText().toString().trim();

                        if(!TextUtils.isEmpty(str3)) {
                            if(!URLUtil.isValidUrl(str3)) {
                                Toast.makeText(AddTriviaActivity.this, "Enter a valid link for image!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        if(TextUtils.isEmpty(str1)) {
                            Toast.makeText(AddTriviaActivity.this,"Please enter small title.",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(TextUtils.isEmpty(str2)) {
                            Toast.makeText(AddTriviaActivity.this,"Please enter question.",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(TextUtils.isEmpty(str5)) {
                            Toast.makeText(AddTriviaActivity.this,"Please enter answer.",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Question question = new Question(str1, str2, str3, str4, str5, str6);
                        list.add(question);
                        questionAdapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(listView);
                        listView.setVisibility(View.VISIBLE);
                        dialog_add.dismiss();
                    }
                });
                dialog_add.show();

            }
        });

        addTrivia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String title = editTitle.getText().toString().trim();
                final String noOfQuestions = editNo.getText().toString().trim();
                final String maxTime = editTime.getText().toString().trim();
                final String Level = level.getSelectedItem().toString();

                if(TextUtils.isEmpty(title)) {
                    Toast.makeText(AddTriviaActivity.this,"Please enter Trivia title.",Toast.LENGTH_SHORT).show();
                    return;
                }if(TextUtils.isEmpty(noOfQuestions)) {
                    Toast.makeText(AddTriviaActivity.this,"Please enter no of questions.",Toast.LENGTH_SHORT).show();
                    return;
                }if(TextUtils.isEmpty(maxTime)) {
                    Toast.makeText(AddTriviaActivity.this,"Please enter max Time.",Toast.LENGTH_SHORT).show();
                    return;
                }

                int size = list.size();
                String Size = Integer.toString(size);
                String winner = "To be announced";
                Calendar calendar = Calendar.getInstance();
                Date date = calendar.getTime();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR, 3);
                date = calendar.getTime();

                if(noOfQuestions.equals(Size)) {
                    TriviaFormat triviaFormat = new TriviaFormat(title, noOfQuestions, maxTime, date, Level, list, winner);

                    firebaseAuth = FirebaseAuth.getInstance();
                    firebaseDatabase = FirebaseDatabase.getInstance();
                    databaseReference = firebaseDatabase.getReference().child("Trivia");
                    databaseReference.setValue(triviaFormat).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(AddTriviaActivity.this, "Trivia added successfully!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(AddTriviaActivity.this, "No of questions incorrect", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public void onBackPressed() {
        builder.setMessage("All the details of trivia saved will be lost. You will have to enter the details again.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("Are you sure you want to exit?");
        alert.show();
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        TriviaFormatAdapter triviaFormatAdapter = (TriviaFormatAdapter) listView.getAdapter();
        if (triviaFormatAdapter == null) {

            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < triviaFormatAdapter.getCount(); i++) {
            View listItem = triviaFormatAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (triviaFormatAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }
}
