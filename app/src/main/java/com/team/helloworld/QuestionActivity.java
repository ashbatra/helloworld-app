package com.team.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class QuestionActivity extends AppCompatActivity {
    ImageView back;
    TextView tv_title, tv_question, tv_link, tv_hint, tv_answer, tv_approach;
    EditText edit_answer;
    Button submit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        getSupportActionBar().hide();

        tv_title = findViewById(R.id.question_title);
        tv_question = findViewById(R.id.question_question);
        tv_link = findViewById(R.id.question_link);
        tv_hint = findViewById(R.id.question_hint);
        tv_answer = findViewById(R.id.question_answer);
        tv_approach = findViewById(R.id.question_approach);
        edit_answer = findViewById(R.id.i_question_answer);
        submit = findViewById(R.id.submit_answer);

        back = findViewById(R.id.back_questionView);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String title = getIntent().getStringExtra("SMALL_TITLE");
        tv_title.setText(title);

        String question = getIntent().getStringExtra("QUESTION");
        tv_question.setText("Question :\n" + question);

        final String link = getIntent().getStringExtra("IMG_LINK");
        if(link.equals("")) {
            tv_link.setVisibility(View.GONE);
        } else {
            tv_link.setText("Link :\n" + link);
            tv_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
                }
            });
        }

        String hint = getIntent().getStringExtra("HINT");
        if(hint.equals("")) {
            tv_hint.setVisibility(View.GONE);
        } else {
            tv_hint.setText("Hint :\n" + hint);
        }

        final String answer = getIntent().getStringExtra("ANSWER");
        tv_answer.setText(answer);

        String approach = getIntent().getStringExtra("APPROACH");
        if(approach.equals("")) {
            tv_approach.setVisibility(View.GONE);
        } else {
            tv_approach.setText("Approach :\n" + approach);
        }
        String use = getIntent().getStringExtra("USE");


        if(use.equals("PLAY")) {
            String pos = getIntent().getStringExtra("POSITION");
            final int position = Integer.parseInt(pos);

            tv_answer.setVisibility(View.GONE);
            edit_answer.setVisibility(View.VISIBLE);
            tv_approach.setVisibility(View.GONE);

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String ans = edit_answer.getText().toString().trim();
                    if(ans.equals(answer)){
                        QuizData.correctOrNOt.set(position, true);
                        Toast.makeText(QuestionActivity.this, "Answer is Correct!!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        QuizData.correctOrNOt.set(position, true);
                        Toast.makeText(QuestionActivity.this, "Wrong Answer!!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        } else if(use.equals("VIEW")) {
            tv_answer.setVisibility(View.VISIBLE);
            edit_answer.setVisibility(View.GONE);
            tv_approach.setVisibility(View.VISIBLE);
            submit.setVisibility(View.GONE);
        }
    }
}
