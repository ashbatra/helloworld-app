package com.team.helloworld;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProjectsFragment extends Fragment {
    Button SignUp;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ProgressBar progressBar;
    RelativeLayout project_logout, project_login;
    FloatingActionButton add_project;
    GridLayout gridLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_projects, container, false);

        progressBar = view.findViewById(R.id.progressBar_project);
        project_logout = view.findViewById(R.id.signed_out);
        project_login = view.findViewById(R.id.signed_in);
        add_project = view.findViewById(R.id.add_project_floating_btn);
        gridLayout = view.findViewById(R.id.project_fields_grid);

        SignUp = view.findViewById(R.id.btn2);
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                TabbedDialog dialogFragment = new TabbedDialog();
                dialogFragment.show(ft,"dialog");

            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        if(firebaseUser == null) {
            progressBar.setVisibility(View.GONE);
            project_logout.setVisibility(View.VISIBLE);
        }
        else {

            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseAuth.getUid());

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

                @SuppressLint("RestrictedApi")
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    String isCore = user.getIsMentor();

                    if (isCore.equals("true")) {
                        add_project.setVisibility(View.VISIBLE);
                        add_project.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // direct to add project activity
                                startActivity(new Intent(getContext(), AddProjectActivity.class));
                            }
                        });

                    }
                    project_login.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getContext(), databaseError.getMessage() , Toast.LENGTH_SHORT).show();
                }
            });

        }
        setgrid();
        return view;
    }

    private void setgrid() {

        for(int i=0;i<gridLayout.getChildCount();i++) {
            CardView cardView = (CardView) gridLayout.getChildAt(i);

            final String ii = Integer.toString(i);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(),ProjectListActivity.class);
                    intent.putExtra("Card Number", ii);
                    startActivity(intent);
                }
            });
        }

    }
}
