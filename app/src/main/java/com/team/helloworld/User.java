package com.team.helloworld;

public class User {
    private String name;
    private String userName;
    private String phone;
    private String eMail;
    private String college;
    private String isTriviaCore;
    private String isMentor;
    private UserQuizData quizData;

    public User() {

    }

    public User(String Name, String phone, String eMail, String college, String isTriviaCore,String isMentor, UserQuizData quizData) {
        this.name = Name;
        this.phone = phone;
        this.eMail = eMail;
        this.college = college;
        this.isTriviaCore = isTriviaCore;
        this.isMentor = isMentor;
        this.userName = createUserName(eMail);
        this.quizData = quizData;
    }

    public String getName() {
        return name;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhone() {
        return phone;
    }

    public String geteMail() {
        return eMail;
    }

    public String getCollege() {
        return college;
    }

    public String getIsMentor() {
        return isMentor;
    }

    public String getIsTriviaCore() {
        return isTriviaCore;
    }

    public UserQuizData getQuizData() {
        return quizData;
    }

    private String createUserName(String email) {
        char[] str1 = email.toCharArray();
        char[] str2 = new char[30];
        for (int i=0; i<email.length(); i++) {
            if (str1[i] != '@') {
                str2[i] = str1[i];
            } else {
                break;
            }
        }
        String string = new String(str2);
        string = string.trim();
        return string;
    }
}
