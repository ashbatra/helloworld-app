package com.team.helloworld;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SettingsFragment extends Fragment {
    String check = "false";
    TextView loggedIn, friend;
    LinearLayout loggedOut;
    LinearLayout chngPswrd, logout, deleteAccount;
    LinearLayout abtUs, contactUs, tandC, feedback, appInfo;
    EditText editFeedback;
    Button cancelbtn, send;
    String editName, editEmail;
    String text = "Hi!";
    Button yes, no, cancel, changepswrd;
    ImageView change;
    EditText pswrd, cnfrmpswrd;
    CheckBox checkBox;
    FirebaseAuth firebaseAuth;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            check = getArguments().getString("CHECK");
            editName = getArguments().getString("NAME");
            editEmail = getArguments().getString("EMAIL");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        loggedIn = view.findViewById(R.id.logged_in);
        loggedOut = view.findViewById(R.id.logged_out);
        chngPswrd = view.findViewById(R.id.settings_change_password);
        logout = view.findViewById(R.id.settings_logout);
        deleteAccount = view.findViewById(R.id.settings_delete_account);
        friend = view.findViewById(R.id.friends);

        abtUs = view.findViewById(R.id.settings_about_us);
        contactUs = view.findViewById(R.id.settings_contact_us);
        tandC = view.findViewById(R.id.settings_terms_privacy);
        feedback = view.findViewById(R.id.settings_feedback);
        appInfo = view.findViewById(R.id.settings_app_info);
        firebaseAuth = FirebaseAuth.getInstance();

        if(editName!=null && editEmail!= null) {
            text = "Hi, " + editName + "\n" + editEmail;
        }

        if(check.equals("false")) {
            loggedOut.setVisibility(View.VISIBLE);
            loggedOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);

                    TabbedDialog dialogFragment = new TabbedDialog();
                    dialogFragment.show(ft,"dialog");
                }
            });

            chngPswrd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Login first!", Toast.LENGTH_SHORT).show();
                }
            });

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Login first!", Toast.LENGTH_SHORT).show();
                }
            });

            deleteAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Login first!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            loggedIn.setVisibility(View.VISIBLE);
            loggedIn.setText(text);

            chngPswrd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    final View view1 = getLayoutInflater().inflate(R.layout.change_password, null);

                    changepswrd = view1.findViewById(R.id.change);
                    cancel = view1.findViewById(R.id.cancel_pswrd);
                    pswrd = view1.findViewById(R.id.ipassword_change);
                    cnfrmpswrd = view1.findViewById(R.id.iConfirmPassword_change);
                    checkBox =  view1.findViewById(R.id.checkbox_chngpassword);

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                            if (!isChecked) {
                                // show password
                                pswrd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                cnfrmpswrd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            } else {
                                // hide password
                                pswrd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                                cnfrmpswrd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            }
                        }
                    });

                    builder.setCancelable(false);
                    builder.setView(view1);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    alertDialog.show();

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    changepswrd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getContext(),"Changing the Password",Toast.LENGTH_SHORT).show();
                            changePassword();
                            alertDialog.dismiss();
                        }
                    });

                }
            });

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    final View view1 = getLayoutInflater().inflate(R.layout.logout, null);

                    yes = view1.findViewById(R.id.yes);
                    no = view1.findViewById(R.id.no);

                    builder.setCancelable(false);
                    builder.setView(view1);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    alertDialog.show();

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getContext(),"Logging Out",Toast.LENGTH_SHORT).show();
                            firebaseAuth.signOut();
                            startActivity(new Intent(getContext(),MainActivity.class));
                            getActivity().finishAffinity();
                        }
                    });

                }
            });

            deleteAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteAccount();
                }
            });

        }

        abtUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening About Us.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://helloworldofficial.in/about")));
            }
        });

        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:?subject=" + ""+ "&body=" + "" + "&to=" + "contact@helloworldofficial.in");
                mailIntent.setData(data);
                Toast.makeText(getContext(), "Opening Gmail...", Toast.LENGTH_SHORT).show();
                startActivity(Intent.createChooser(mailIntent, "Send mail..."));
            }
        });

        tandC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening Privacy Policy.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://helloworldofficial.in/privacy-policy")));
            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                View view1 = getLayoutInflater().inflate(R.layout.feedback, null);

                editFeedback = view1.findViewById(R.id.feedback);
                cancelbtn = view1.findViewById(R.id.cancel_fdb);
                send = view1.findViewById(R.id.send_fdb);

                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


                cancelbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String fdb = "Hi,\nI'm using the HelloWorld app and my feedback for the app is :\n" + editFeedback.getText().toString().trim();
                        if(TextUtils.isEmpty(fdb)) {
                            Toast.makeText(getContext(), "Feedback can't be Empty!" + fdb, Toast.LENGTH_SHORT).show();
                        } else {
                            Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri.parse("mailto:?subject=" + "FeedBack for HelloWorld App"+ "&body=" + fdb + "&to=" + "developers@helloworldofficial.in");
                            mailIntent.setData(data);
                            Toast.makeText(getContext(), "Please Wait..", Toast.LENGTH_SHORT).show();
                            startActivity(Intent.createChooser(mailIntent, "Send mail..."));
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        appInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AppInfoActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
            }
        });

        friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent(Intent.ACTION_SEND);
                final String appPackagename = getActivity().getApplicationContext().getPackageName();
                String strAppLink = "";

                try {
                    strAppLink = "https://play.google.com/store/apps/details?id=" + appPackagename;
                } catch (android.content.ActivityNotFoundException anfe) {
                    strAppLink = "https://play.google.com/store/apps/details?id=" + appPackagename;
                }
                intent4.setType("text/link");
                String shareBody = "Hey! Check out this awesome app HelloWorld for Tech-trivia and Software Project development guide : " + strAppLink;

                String shareSub = "HelloWorld";
                intent4.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent4.putExtra(Intent.EXTRA_TEXT,shareBody);
                Toast.makeText(getContext(), "Sharing App.", Toast.LENGTH_SHORT).show();
                startActivity(Intent.createChooser(intent4,"Share Using"));
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
    }

    public void deleteAccount() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final View view1 = getLayoutInflater().inflate(R.layout.delete, null);

        yes = view1.findViewById(R.id.logout);
        no = view1.findViewById(R.id.cancel_logout);

        builder.setCancelable(false);
        builder.setView(view1);
        final AlertDialog alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(getContext(),"Account Deleted",Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getContext(),SplashActivity.class));
                            getActivity().finishAffinity();
                        } else {
                            Toast.makeText(getContext(),task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }

    public void changePassword() {

        String password = pswrd.getText().toString().trim();
        String CnfrmPassword = cnfrmpswrd.getText().toString().trim();

        if(TextUtils.isEmpty(password)) {
            Toast.makeText(getContext(),"Please enter password.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(CnfrmPassword)) {
            Toast.makeText(getContext(),"Please re-enter your password.",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.length()<6||CnfrmPassword.length()<6) {
            Toast.makeText(getContext(),"Minimum 6 characters Required!",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!password.equals(CnfrmPassword)) {
            Toast.makeText(getContext(),"Passwords don't Match!",Toast.LENGTH_SHORT).show();
            return;
        }
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        //create user and put name,email,password and phone in database
        firebaseUser.updatePassword(password).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getContext(), "Password changed Successfully!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
