package com.team.helloworld;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ProjectAdapter extends ArrayAdapter<Project>  {

    private Activity context;
    private List<Project> projects;

    public ProjectAdapter(Activity context, List<Project> projects) {
        super(context, R.layout.projects_list_item_layout, projects);

        this.context = context;
        this.projects = projects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();

        View listViewItem = layoutInflater.inflate(R.layout.projects_list_item_layout, null, true);

        TextView title = listViewItem.findViewById(R.id.p_title);
        TextView mentor = listViewItem.findViewById(R.id.p_mentor);
        TextView desc = listViewItem.findViewById(R.id.p_desc);
        TextView nooflinks = listViewItem.findViewById(R.id.p_no_of_links);

        title.setText(projects.get(position).getTitle());
        mentor.setText("* Mentor : " + projects.get(position).getMentor());
        desc.setText(projects.get(position).getDescription());
        nooflinks.setText("* Total Links : " + projects.get(position).getLinks().size());

        return listViewItem;
    }
}

