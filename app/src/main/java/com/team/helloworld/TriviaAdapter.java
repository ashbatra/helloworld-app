package com.team.helloworld;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.DateFormat;
import java.util.List;

public class TriviaAdapter extends ArrayAdapter<TriviaFormat> {

    private Activity context;
    private List<TriviaFormat> triviaFormats;

    public TriviaAdapter(Activity context, List<TriviaFormat> triviaFormats) {
        super(context, R.layout.trivia_list_item_layout, triviaFormats);

        this.context = context;
        this.triviaFormats = triviaFormats;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();

        View listViewItem = layoutInflater.inflate(R.layout.trivia_list_item_layout, null, true);

        TextView title = listViewItem.findViewById(R.id.t_title);
        TextView noofques = listViewItem.findViewById(R.id.t_no_ques);
        TextView time = listViewItem.findViewById(R.id.t_max_time);
        TextView level = listViewItem.findViewById(R.id.t_level);
        TextView conducted = listViewItem.findViewById(R.id.t_conducted_on);
        TextView winner = listViewItem.findViewById(R.id.t_winner);

        String No = "*  No. of Questions : " + triviaFormats.get(position).getNoOfQues();
        String Time = "*  Time allotted : " + triviaFormats.get(position).getTime() + " mins.";
        String Level = "*  Level : " + triviaFormats.get(position).getLevel();
        String Conduct = "*  Conducted on : " + DateFormat.getInstance().format(triviaFormats.get(position).getActiveTill());
        String Winner = "*  Winner : " + triviaFormats.get(position).getWinner();

        title.setText(triviaFormats.get(position).getTitle());
        noofques.setText(No);
        time.setText(Time);
        level.setText(Level);
        conducted.setText(Conduct);
        winner.setText(Winner);

        return listViewItem;
    }
}
