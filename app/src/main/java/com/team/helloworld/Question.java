package com.team.helloworld;

public class Question {

    private String smallTitle;
    private String question;
    private String imageUrl;
    private String hint;
    private String answer;
    private String approach;

    public Question() {

    }

    public Question(String smallTitle, String question, String imageUrl, String hint, String answer, String approach) {
        this.smallTitle = smallTitle;
        this.question = question;
        this.imageUrl = imageUrl;
        this.hint = hint;
        this.answer = answer;
        this.approach = approach;
    }

    public String getSmallTitle() {
        return smallTitle;
    }

    public String getQuestion() {
        return question;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getHint() {
        return hint;
    }

    public String getAnswer() {
        return answer;
    }

    public String getApproach() {
        return approach;
    }
}
