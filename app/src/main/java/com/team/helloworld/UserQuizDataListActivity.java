package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UserQuizDataListActivity extends AppCompatActivity {
    ProgressBar progressBar;
    List<UserQuizData> userQuizDataList;
    ListView listView;
    AlertDialog.Builder builder;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_quiz_data_list);

        getSupportActionBar().hide();

        progressBar = findViewById(R.id.progressBar_userquizdata_list);
        builder = new AlertDialog.Builder(this);
        back = findViewById(R.id.back_btn_user_quizdata);
        listView = findViewById(R.id.user_quizdata_list);
        userQuizDataList = new ArrayList<>();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userQuizDataList.clear();
                for(DataSnapshot userDataSnapshot : dataSnapshot.getChildren()) {
                    UserQuizData userQuizData = userDataSnapshot.child("quizData").getValue(UserQuizData.class);
                    if(userQuizData!=null) {
                        userQuizDataList.add(userQuizData);
                    }
                    listView.setVisibility(View.VISIBLE);
                }

                UserQuizDataAdapter userQuizDataAdapter = new UserQuizDataAdapter(UserQuizDataListActivity.this, userQuizDataList);
                listView.setAdapter(userQuizDataAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        builder.setMessage(userQuizDataList.get(position).getUserName() + " will be announced as winner of Trivia.")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child("Trivia").child("winner");
                                        databaseReference1.setValue(userQuizDataList.get(position).getUserName()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()) {
                                                    Toast.makeText(UserQuizDataListActivity.this, "Winner added", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }
                                                else {
                                                    Toast.makeText(UserQuizDataListActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.setTitle("Are you sure?");
                        alert.show();
                    }
                });

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(UserQuizDataListActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
