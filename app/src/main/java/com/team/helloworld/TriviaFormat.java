package com.team.helloworld;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TriviaFormat {
    private Date activeTill;
    private Date expDate;
    private String level;
    private String noOfQues;
    private List<Question> questions;
    private String time;
    private String title;
    private String winner;

    public TriviaFormat() {

    }

    public TriviaFormat(String Title, String noOfQues, String Time, Date activeTill, String Level, List<Question> questions, String winner) {
        title = Title;
        this.noOfQues = noOfQues;
        time = Time;
        this.activeTill = activeTill;
        level = Level;
        this.questions = questions;
        this.winner = winner;

        Calendar c = Calendar.getInstance();
        c.setTime(activeTill);
        c.add(Calendar.DATE, 1);
        Date date = c.getTime();

        this.expDate = date;
    }

    public String getTitle() {
        return title;
    }

    public String getNoOfQues() {
        return noOfQues;
    }

    public String getTime() {
        return time;
    }

    public Date getActiveTill() {
        return activeTill;
    }

    public Date getExpDate() {
        return expDate;
    }

    public String getLevel() {
        return level;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public String getWinner() {
        return winner;
    }
}
