package com.team.helloworld;

import java.util.Date;

public class UserQuizData {
    private String userName;
    private Date start;
    private Date finish;
    private int correct;

    public UserQuizData() {

    }

    public UserQuizData(String userName, Date start, Date finish, int correct) {
        this.userName = userName;
        this.start = start;
        this.finish = finish;
        this.correct = correct;
    }

    public String getUserName() {
        return userName;
    }

    public Date getStart() {
        return start;
    }

    public Date getFinish() {
        return finish;
    }

    public int getCorrect() {
        return correct;
    }
}
