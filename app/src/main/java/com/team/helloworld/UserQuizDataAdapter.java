package com.team.helloworld;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.DateFormat;
import java.util.List;

public class UserQuizDataAdapter extends ArrayAdapter<UserQuizData>  {

    private Activity context;
    private List<UserQuizData> userQuizDataList;

    public UserQuizDataAdapter(Activity context, List<UserQuizData> userQuizData) {
        super(context, R.layout.user_quiz_data_list_item_layout, userQuizData);

        this.context = context;
        this.userQuizDataList = userQuizData;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();

        View listViewItem = layoutInflater.inflate(R.layout.user_quiz_data_list_item_layout, null, true);

        TextView username = listViewItem.findViewById(R.id.username);
        TextView started = listViewItem.findViewById(R.id.started);
        TextView finished = listViewItem.findViewById(R.id.finished);
        TextView correct = listViewItem.findViewById(R.id.correct);

        username.setText(userQuizDataList.get(position).getUserName());
        started.setText("*  Started At : " + DateFormat.getInstance().format(userQuizDataList.get(position).getStart()));
        finished.setText("*  Finished At : " + DateFormat.getInstance().format(userQuizDataList.get(position).getFinish()));
        correct.setText("*  Correct Replies : " + userQuizDataList.get(position).getCorrect());

        return listViewItem;
    }
}

