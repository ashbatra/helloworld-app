package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddProjectActivity extends AppCompatActivity {
    ImageView back;
    Button addLink, addProject, add, cancel;
    EditText title, mentor, desc, link;
    Spinner field, linkType;
    List<Link> linkList;
    AlertDialog.Builder builder;
    ListView listView;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        getSupportActionBar().hide();

        linkList = new ArrayList<>();
        back = findViewById(R.id.back_btn_project);
        addLink = findViewById(R.id.add_link);
        addProject = findViewById(R.id.add_project_btn);
        title = findViewById(R.id.iproject_title);
        mentor = findViewById(R.id.imentor);
        builder = new AlertDialog.Builder(this);
        listView = findViewById(R.id.links_list);
        desc = findViewById(R.id.idesc);
        field = findViewById(R.id.ifield);
        final LinkAdapter linkAdapter = new LinkAdapter(AddProjectActivity.this, linkList);
        listView.setAdapter(linkAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.setMessage("All the details of project saved will be lost. You will have to enter the details again.")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.setTitle("Are you sure you want to exit?");
                alert.show();
            }
        });

        addLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog_add = new Dialog(AddProjectActivity.this);
                dialog_add.setContentView(getLayoutInflater().inflate(R.layout.link_dialog, null));
                dialog_add.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                link = dialog_add.findViewById(R.id.i_link);
                linkType = dialog_add.findViewById(R.id.ilinkType);

                add= dialog_add.findViewById(R.id.add_link_btn);
                cancel = dialog_add.findViewById(R.id.cancel_link_btn);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_add.dismiss();
                    }
                });

                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String str = link.getText().toString().trim();
                        String type = linkType.getSelectedItem().toString();

                        if(!TextUtils.isEmpty(str)) {
                            if(!URLUtil.isValidUrl(str)) {
                                Toast.makeText(AddProjectActivity.this, "Enter a valid link for image!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        if(TextUtils.isEmpty(str)) {
                            Toast.makeText(AddProjectActivity.this,"Please enter small title.",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Link link = new Link(type, str);
                        linkList.add(link);
                        linkAdapter.notifyDataSetChanged();
                        setListViewHeightBasedOnChildren(listView);
                        listView.setVisibility(View.VISIBLE);
                        dialog_add.dismiss();
                    }
                });
                dialog_add.show();

            }
        });

        addProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String editTitle = title.getText().toString().trim();
                final String editMentor = mentor.getText().toString().trim();
                final String editDesc = desc.getText().toString().trim();
                final String editField = field.getSelectedItem().toString();

                if(TextUtils.isEmpty(editTitle)) {
                    Toast.makeText(AddProjectActivity.this,"Please enter Project title.",Toast.LENGTH_SHORT).show();
                    return;
                }if(TextUtils.isEmpty(editMentor)) {
                    Toast.makeText(AddProjectActivity.this,"Please enter mentor.",Toast.LENGTH_SHORT).show();
                    return;
                }if(TextUtils.isEmpty(editDesc)) {
                    Toast.makeText(AddProjectActivity.this,"Please enter Project Description.",Toast.LENGTH_SHORT).show();
                    return;
                }

                int size = linkList.size();
                if(size == 0) {
                    Toast.makeText(AddProjectActivity.this,"Please enter at least one Link.",Toast.LENGTH_SHORT).show();
                    return;
                }

                Project project = new Project(editTitle, editDesc, editField, editMentor, linkList);

                    firebaseAuth = FirebaseAuth.getInstance();
                    firebaseDatabase = FirebaseDatabase.getInstance();
                    databaseReference = firebaseDatabase.getReference().child("Projects").child(editField).child(editTitle);
                    databaseReference.setValue(project).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(AddProjectActivity.this, "Project added successfully!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
            }
        });
    }

    @Override
    public void onBackPressed() {
        builder.setMessage("All the details of project saved will be lost. You will have to enter the details again.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("Are you sure you want to exit?");
        alert.show();
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        LinkAdapter linkAdapter  = (LinkAdapter) listView.getAdapter();
        if (linkAdapter == null) {

            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < linkAdapter.getCount(); i++) {
            View listItem = linkAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (linkAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }
}
