package com.team.helloworld;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

public class HomeFragment extends Fragment implements View.OnClickListener {
    CardView events, about, gallery, web, share, feedback;
    ImageView fb,insta,whatsapp,linkedin,mail,website;
    RatingBar ratingBar;
    Button submit, later, cancel, send;
    EditText editFeedback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        events = view.findViewById(R.id.events_card);
        about = view.findViewById(R.id.aboutus_card);
        gallery = view.findViewById(R.id.gallery_card);
        web = view.findViewById(R.id.website_card);
        share = view.findViewById(R.id.shareapp_card);
        feedback = view.findViewById(R.id.feedback_card);

        fb = view.findViewById(R.id.fb1);
        insta = view.findViewById(R.id.insta1);
        whatsapp = view.findViewById(R.id.what1);
        linkedin = view.findViewById(R.id.lin1);
        mail = view.findViewById(R.id.mail1);
        website = view.findViewById(R.id.web1);

        fb.setOnClickListener(this);
        insta.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        mail.setOnClickListener(this);
        website.setOnClickListener(this);

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening Website...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), WebsiteActivity.class);
                intent.putExtra("URL", "https://helloworldofficial.in");
                startActivity(intent);
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening About Us.", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(getContext(), WebsiteActivity.class);
                intent1.putExtra("URL", "https://helloworldofficial.in/about");
                startActivity(intent1);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent(Intent.ACTION_SEND);
                final String appPackagename = getActivity().getApplicationContext().getPackageName();
                String strAppLink = "";

                try {
                    strAppLink = "https://play.google.com/store/apps/details?id=" + appPackagename;
                } catch (android.content.ActivityNotFoundException anfe) {
                    strAppLink = "https://play.google.com/store/apps/details?id=" + appPackagename;
                }
                intent4.setType("text/link");
                String shareBody = "Hey! Check out this awesome app HelloWorld for Tech-trivia and Software Project development guide : " + strAppLink;

                String shareSub = "HelloWorld";
                intent4.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                intent4.putExtra(Intent.EXTRA_TEXT,shareBody);
                Toast.makeText(getContext(), "Sharing App.", Toast.LENGTH_SHORT).show();
                startActivity(Intent.createChooser(intent4,"Share Using"));
            }
        });

        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening Events.", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(getContext(), WebsiteActivity.class);
                intent1.putExtra("URL", "https://helloworldofficial.in/upcoming-and-past-events");
                startActivity(intent1);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Opening Gallery.", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(getContext(), WebsiteActivity.class);
                intent1.putExtra("URL", "https://helloworldofficial.in/gallery");
                startActivity(intent1);
            }
        });
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                final View view = getLayoutInflater().inflate(R.layout.rate, null);

                ratingBar = view.findViewById(R.id.ratingbar);
                submit = view.findViewById(R.id.submitrating);
                later = view.findViewById(R.id.later);

                alert.setView(view);
                final AlertDialog alertDialog = alert.create();
                alertDialog.setCancelable(false);
                alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


                later.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        float rating = ratingBar.getRating();

                        if(rating <= 3.0) {

                            alertDialog.dismiss();
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            View view1 = getLayoutInflater().inflate(R.layout.feedback, null);

                            editFeedback = view1.findViewById(R.id.feedback);
                            cancel = view1.findViewById(R.id.cancel_fdb);
                            send = view1.findViewById(R.id.send_fdb);

                            builder.setView(view1);
                            final AlertDialog dialog = builder.create();
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setCancelable(false);
                            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                            send.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String fdb = "Hi,\nI'm using the HelloWorld app and my feedback for the app is :\n" + editFeedback.getText().toString().trim();
                                    if(TextUtils.isEmpty(fdb)) {
                                        Toast.makeText(getContext(), "Feedback can't be Empty!" + fdb, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                                        Uri data = Uri.parse("mailto:?subject=" + "FeedBack for HelloWorld App"+ "&body=" + fdb + "&to=" + "developers@helloworldofficial.in");
                                        mailIntent.setData(data);
                                        Toast.makeText(getContext(), "Please Wait..", Toast.LENGTH_SHORT).show();
                                        startActivity(Intent.createChooser(mailIntent, "Send mail..."));
                                    }
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();

                        }

                        else {
                            Toast.makeText(getContext(), "Opening PlayStore...", Toast.LENGTH_SHORT).show();
                            final String appPackagename = getActivity().getApplicationContext().getPackageName();
                            Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appPackagename));
                            startActivity(intent1);
                            alertDialog.dismiss();
                        }
                    }
                });

                alertDialog.show();
            }
        });
        SliderView sliderView = view.findViewById(R.id.imageSlider);

        SliderAdapterExample adapter = new SliderAdapterExample(getContext());

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();



        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fb1 :
                Toast.makeText(getContext(), "Opening Facebook...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/helloworldteam/")));
                break;

            case R.id.insta1 :
                Toast.makeText(getContext(), "Opening Instagram...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/official_hello_world/")));
                break;

            case R.id.what1 :
                Toast.makeText(getContext(), "Opening Whatsapp...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://chat.whatsapp.com/BmAolfGm1Y55NoDfxPN2YY")));
                break;

            case R.id.lin1 :
                Toast.makeText(getContext(), "Opening LinkedIn...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/company/helloworldofficial/")));
                break;

            case R.id.mail1 :
                Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:?subject=" + ""+ "&body=" + "" + "&to=" + "contact@helloworldofficial.in");
                mailIntent.setData(data);
                Toast.makeText(getContext(), "Opening Gmail...", Toast.LENGTH_SHORT).show();
                startActivity(Intent.createChooser(mailIntent, "Send mail..."));
                break;

            case R.id.web1 :
                Toast.makeText(getContext(), "Opening Website...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://helloworldofficial.in")));
                break;
        }
    }


}
