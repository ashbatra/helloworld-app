package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProjectActivity extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    LinearLayout projectCard;
    ProgressBar progressBar;
    TextView error;
    ImageView back;
    ListView listView;
    List<Link> linkList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_project);

        getSupportActionBar().hide();
        String field = getIntent().getStringExtra("FIELD");
        String p_title = getIntent().getStringExtra("PROJECT_TITLE");

        listView = findViewById(R.id.links_list);
        back = findViewById(R.id.back_projectView);
        error = findViewById(R.id.error);
        linkList = new ArrayList<>();
        progressBar = findViewById(R.id.progressBar_projectView);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final TextView title = findViewById(R.id.project_title);
        final TextView mentor = findViewById(R.id.project_mentor);
        final TextView desc = findViewById(R.id.project_desc);
        final TextView nooflinks = findViewById(R.id.project_no_of_links);

        projectCard = findViewById(R.id.project_data_card);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Projects").child(field).child(p_title);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Project project = dataSnapshot.getValue(Project.class);

                title.setText(project.getTitle());
                mentor.setText("*  Mentor : " + project.getMentor());
                desc.setText(project.getDescription());
                nooflinks.setText("*  Total Links : " + project.getLinks().size());

                linkList = project.getLinks();

                final LinkAdapter linkAdapter = new LinkAdapter(ProjectActivity.this, linkList);
                listView.setAdapter(linkAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String type = linkList.get(position).getType();
                        if(type.equals("YouTube")) {
                            Intent intent1 = new Intent(ProjectActivity.this, YoutubeVideoPlayActivity.class);
                            intent1.putExtra("VIDEO_URL", linkList.get(position).getLink());
                            startActivity(intent1);
                        } else {
                            Intent intent2 = new Intent(ProjectActivity.this, WebsiteActivity.class);
                            intent2.putExtra("URL", linkList.get(position).getLink());
                            startActivity(intent2);
                        }
                    }
                });

                projectCard.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ProjectActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
            }
        });

    }
}
