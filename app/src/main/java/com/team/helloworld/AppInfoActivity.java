package com.team.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class AppInfoActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    ImageView fb,insta,whatsapp,linkedin,mail,website;
    ImageView g1, g2, g3, l1, l2, l3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);
        getSupportActionBar().hide();

        fb = findViewById(R.id.fb2);
        insta = findViewById(R.id.insta2);
        whatsapp = findViewById(R.id.what2);
        linkedin = findViewById(R.id.lin2);
        mail = findViewById(R.id.mail2);
        website = findViewById(R.id.web2);

        fb.setOnClickListener(this);
        insta.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        mail.setOnClickListener(this);
        website.setOnClickListener(this);

        g1 = findViewById(R.id.git1);
        g2 = findViewById(R.id.git2);
        g3 = findViewById(R.id.git3);
        l1 = findViewById(R.id.linkedin1);
        l2 = findViewById(R.id.linkedin2);
        l3 = findViewById(R.id.linkedin3);

        g1.setOnClickListener(this);
        g2.setOnClickListener(this);
        g3.setOnClickListener(this);
        l1.setOnClickListener(this);
        l2.setOnClickListener(this);
        l3.setOnClickListener(this);

        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.slide_out);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fb2 :
                Toast.makeText(AppInfoActivity.this, "Opening Facebook...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/helloworldteam/")));
                break;

            case R.id.insta2 :
                Toast.makeText(AppInfoActivity.this, "Opening Instagram...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/official_hello_world/")));
                break;

            case R.id.what2 :
                Toast.makeText(AppInfoActivity.this, "Opening Whatsapp...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://chat.whatsapp.com/BmAolfGm1Y55NoDfxPN2YY")));
                break;

            case R.id.lin2 :
                Toast.makeText(AppInfoActivity.this, "Opening LinkedIn...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/company/helloworldofficial/")));
                break;

            case R.id.mail2 :
                Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:?subject=" + ""+ "&body=" + "" + "&to=" + "contact@helloworldofficial.in");
                mailIntent.setData(data);
                Toast.makeText(AppInfoActivity.this, "Opening Gmail...", Toast.LENGTH_SHORT).show();
                startActivity(Intent.createChooser(mailIntent, "Send mail..."));
                break;

            case R.id.web2 :
                Toast.makeText(AppInfoActivity.this, "Opening Website...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://helloworldofficial.in")));
                break;

            case R.id.linkedin1 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/ashish-batra-4b8a5a174/")));
                break;

            case R.id.linkedin2 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/siddharth-narula-70a78498/")));
                break;

            case R.id.linkedin3 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/sukirt-kaur-01b523191/")));
                break;

            case R.id.git1 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/ashbatra")));
                break;

            case R.id.git2 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/siddharthnarula1234")));
                break;

            case R.id.git3 :
                Toast.makeText(AppInfoActivity.this, "Please Wait.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/sukirt01")));
                break;

        }
    }


}
