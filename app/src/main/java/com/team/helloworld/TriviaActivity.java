package com.team.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TriviaActivity extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ProgressBar progressBar;
    RelativeLayout gotData;
    TextView triviaTitle, counter, donotQuit;
    ListView listView;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trivia);
        getSupportActionBar().hide();

        progressBar = findViewById(R.id.progressBar_trivia_activity);
        gotData = findViewById(R.id.got_trivia_details);
        triviaTitle = findViewById(R.id.trivia_title);
        counter = findViewById(R.id.counter_txt);
        donotQuit = findViewById(R.id.play_txt_only);
        listView = findViewById(R.id.trivia_question_list);
        submit = findViewById(R.id.submit_quiz_answers);

        String use = getIntent().getStringExtra("USE_OF_ACTIVITY");

        if(use.equals("View")) {

            String trivia_name = getIntent().getStringExtra("TRIVIA_NAME");
            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference().child("Previous Trivia").child(trivia_name);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    TriviaFormat triviaFormat = dataSnapshot.getValue(TriviaFormat.class);
                    setTrivia(triviaFormat, false);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(TriviaActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }

        else if(use.equals("Play")) {
            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference().child("Trivia");

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    TriviaFormat triviaFormat = dataSnapshot.getValue(TriviaFormat.class);
                    setTrivia(triviaFormat, true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(TriviaActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }

    }

    private void setTrivia(final TriviaFormat trivia, boolean isPlay) {
        if(trivia == null) {
            Toast.makeText(this, "There's some error, try later", Toast.LENGTH_SHORT).show();
            finish();
        }

        String title = trivia.getTitle();
        String minutes = trivia.getTime();

        triviaTitle.setText(title);
        QuestionAdapter questionAdapter = new QuestionAdapter(TriviaActivity.this, trivia.getQuestions());
        listView.setAdapter(questionAdapter);
        progressBar.setVisibility(View.GONE);

        if(isPlay) {

            Calendar calendar = Calendar.getInstance();
            Date start = calendar.getTime();
            QuizData.setStartedAt(start);

            int min = Integer.parseInt(minutes);
            int millis = min*60000;
            final CountDownTimer countDownTimer = new CountDownTimer(millis, 60000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    counter.setText("Min Remaining :\n" + millisUntilFinished/60000);
                }

                @Override
                public void onFinish() {
                    submitQuiz();
                }
            };
            countDownTimer.start();
            QuizData.setCorrectOrNOt(Integer.parseInt(trivia.getNoOfQues()));

            gotData.setVisibility(View.VISIBLE);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String smallTitle = trivia.getQuestions().get(position).getSmallTitle();
                    String question = trivia.getQuestions().get(position).getQuestion();
                    String imageLink = trivia.getQuestions().get(position).getImageUrl();
                    String hint = trivia.getQuestions().get(position).getHint();
                    String answer = trivia.getQuestions().get(position).getAnswer();
                    String approach = trivia.getQuestions().get(position).getApproach();

                    Intent intent = new Intent(TriviaActivity.this, QuestionActivity.class);
                    intent.putExtra("SMALL_TITLE", smallTitle);
                    intent.putExtra("QUESTION", question);
                    intent.putExtra("IMG_LINK", imageLink);
                    intent.putExtra("HINT", hint);
                    intent.putExtra("ANSWER", answer);
                    intent.putExtra("APPROACH", approach);
                    intent.putExtra("USE", "PLAY");
                    intent.putExtra("POSITION", Integer.toString(position));
                    startActivity(intent);

                }
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitQuiz();
                    countDownTimer.cancel();
                }
            });

        } else {

            counter.setVisibility(View.INVISIBLE);
            donotQuit.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            gotData.setVisibility(View.VISIBLE);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String smallTitle = trivia.getQuestions().get(position).getSmallTitle();
                    String question = trivia.getQuestions().get(position).getQuestion();
                    String imageLink = trivia.getQuestions().get(position).getImageUrl();
                    String hint = trivia.getQuestions().get(position).getHint();
                    String answer = trivia.getQuestions().get(position).getAnswer();
                    String approach = trivia.getQuestions().get(position).getApproach();

                    Intent intent = new Intent(TriviaActivity.this, QuestionActivity.class);
                    intent.putExtra("SMALL_TITLE", smallTitle);
                    intent.putExtra("QUESTION", question);
                    intent.putExtra("IMG_LINK", imageLink);
                    intent.putExtra("HINT", hint);
                    intent.putExtra("ANSWER", answer);
                    intent.putExtra("APPROACH", approach);
                    intent.putExtra("USE", "VIEW");
                    startActivity(intent);
                }
            });
        }
    }

    public void submitQuiz() {
        Calendar calendar = Calendar.getInstance();
        final Date finish = calendar.getTime();
        QuizData.setFinishedAt(finish);

        int correct = 0;
        List<Boolean> list = QuizData.getCorrectOrNOt();
        for (int i=0; i<list.size();i++) {
            if(list.get(i).equals(true)) {
                correct++;
            }
        }

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("Users").child(FirebaseAuth.getInstance().getUid());

        final int finalCorrect = correct;
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                UserQuizData userQuizData = new UserQuizData(user.getName(), QuizData.startedAt, QuizData.finishedAt, finalCorrect);

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getUid()).child("quizData");
                databaseReference.setValue(userQuizData).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(TriviaActivity.this, "Quiz Answers Submitted Successfuly", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(TriviaActivity.this, MainActivity.class));
                            finish();
                        }
                        else {
                            Toast.makeText(TriviaActivity.this, "Quiz Submission Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(TriviaActivity.this, "Quiz Submission Failed! " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }
}
